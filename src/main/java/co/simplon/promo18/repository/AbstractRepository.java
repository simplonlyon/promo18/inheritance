package co.simplon.promo18.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<T, I> {

    private String tableName;
    private String[] persistParams;

    public AbstractRepository(String tableName, String[] persistParams) {
        this.tableName = tableName;
        this.persistParams = persistParams;
    }

    public List<T> findAll() {
        List<T> list = new ArrayList<>();
        try (Connection cnx = Database.getConnection()) {
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM "+tableName);

            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                list.add(sqlToEntity(rs));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Repository error", e);
        }

        return list;
    }

    public T findById(I id) {
        try (Connection cnx = Database.getConnection()) {
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM "+tableName+" WHERE id=?");
            // stmt.setInt(1, id);
            stmt.setObject(1, id);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return sqlToEntity(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException("Repository error", e);
        }
    }

    public void persist(T entity) {
        try (Connection cnx = Database.getConnection()) {
            PreparedStatement stmt = cnx.prepareStatement("INSERT INTO "+tableName+" ("+String.join(",",persistParams)+ ") VALUES ("+"?,".repeat(persistParams.length-1)+"?)");
            
            setParameters(stmt, entity);

            stmt.executeUpdate();
            
            
        } catch (SQLException e) {
            throw new RuntimeException("Repository error", e);
        }
    }

    protected abstract void setParameters(PreparedStatement stmt, T entity) throws SQLException;

    protected abstract T sqlToEntity(ResultSet rs) throws SQLException;
}
