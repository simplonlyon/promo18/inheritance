package co.simplon.promo18.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entity.Person;

public class PersonRepository extends AbstractRepository<Person, Integer> {

    public PersonRepository() {
        super("person", new String[]{"name", "age"});
    }

    @Override
    protected Person sqlToEntity(ResultSet rs) throws SQLException {
        Person person = new Person(
            rs.getInt("id"), 
            rs.getString("name"), 
            rs.getInt("age"));
        return person;
    }

    @Override
    protected void setParameters(PreparedStatement stmt, Person entity) throws SQLException {
        stmt.setString(1, entity.getName());
        stmt.setInt(2, entity.getAge());
        
    }
    
}
