package co.simplon.promo18;

import co.simplon.promo18.repository.PersonRepository;
import entity.Person;

public class AppRepo {
    public static void main(String[] args) {
        PersonRepository repo = new PersonRepository();

        repo.persist(new Person("testouille", 56));

        for(Person person : repo.findAll()) {
            System.out.println(person.getName());
        }

        System.out.println(repo.findById(1).getName());
    }
}
