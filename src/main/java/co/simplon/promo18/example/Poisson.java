package co.simplon.promo18.example;

public class Poisson extends Animal implements IPond{

    public Poisson() {
        super("poiscaille");
        
    }

    public void nager() {
        System.out.println("le poisson nage avec grâce");
    }

    @Override
    public void pondre() {
        System.out.println("retourne à son lieu de naissance pour pondre et crever");
        
    }
    
}
