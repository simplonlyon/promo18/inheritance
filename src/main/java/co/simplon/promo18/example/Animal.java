package co.simplon.promo18.example;

public class Animal {
    protected String nom;
    

    public Animal(String nom) {
        this.nom = nom;
    }


    public void manger() {
        System.out.println(nom + " mange un truc");
    }
}
