package co.simplon.promo18.example;

public class Dog extends Animal{
    private String race;
    
    public Dog(String race) {
        super("chien");
        this.race = race;
    }


    public void aboyer() {
        System.out.println(nom+" fait waf");
    }

    @Override
    public void manger() {
        System.out.println(nom+",qui est un "+race+" mange spécifiquement des croquettes");
    }
}
