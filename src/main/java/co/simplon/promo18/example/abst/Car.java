package co.simplon.promo18.example.abst;

public class Car extends Vehicle {
    private int gas;

    @Override
    protected int propulsion() {
        if(gas > 5) {
            gas -= 5;
            return 50;
        }    
        return 0;
    }
    
}
