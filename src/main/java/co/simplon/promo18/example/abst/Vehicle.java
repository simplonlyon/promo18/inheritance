package co.simplon.promo18.example.abst;

public abstract class Vehicle {
    protected int currentSpeed = 0;

    public void move() {
        System.out.println("the vehicle is moving");
        currentSpeed = propulsion();
    }

    protected abstract int propulsion();

}
