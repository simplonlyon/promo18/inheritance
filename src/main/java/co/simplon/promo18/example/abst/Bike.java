package co.simplon.promo18.example.abst;

public class Bike extends Vehicle {

    @Override
    protected int propulsion() {
        System.out.println("pedaling...");
        return 20;
    }
    
}
