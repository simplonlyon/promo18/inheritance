package co.simplon.promo18;

import co.simplon.promo18.example.abst.Bike;
import co.simplon.promo18.example.abst.Car;
import co.simplon.promo18.example.abst.Vehicle;

public class AppAbst {
    public static void main(String[] args) {
        Vehicle vehicle = new Bike();

        vehicle.move();

        vehicle = new Car();

        vehicle.move();
    }
}
