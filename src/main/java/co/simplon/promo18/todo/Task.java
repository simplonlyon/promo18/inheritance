package co.simplon.promo18.todo;

public class Task {
    private String label;
    private boolean done;

    public Task(String label) {
        this.label = label;
        this.done = false;
    }

    public void check() {
        done = !done;
    }

    @Override
    public String toString() {
        if(done) {
            return "☒ "+label;
        }
        return "☐ "+label;
    }

    

}
