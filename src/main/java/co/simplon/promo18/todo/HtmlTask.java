package co.simplon.promo18.todo;

public class HtmlTask extends Task {

    public HtmlTask(String label) {
        super(label);
    }

    @Override
    public String toString() {
        return "<p>"+super.toString()+"</p>";
    }
    
}
