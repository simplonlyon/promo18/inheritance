package co.simplon.promo18.todo;

import java.util.ArrayList;
import java.util.List;

import co.simplon.promo18.todo.interfaces.ITextTransform;

public class TodoList {
    private List<Task> tasks = new ArrayList<>();
    private boolean html;
    private List<ITextTransform> transformers = new ArrayList<>();

    public TodoList(boolean html) {
        this.html = html;
    }

    public void addTask(String label) {
        for (ITextTransform transformer : transformers) {
            label = transformer.transform(label);
        }

        Task task;
        if(html) {
            task = new HtmlTask(label);
        }else {
            task = new Task(label);
        }
        tasks.add(task);
    }

    public void addTransformer(ITextTransform transformer) {
        transformers.add(transformer);

    }

    public void display() {
        for (Task task : tasks) {
            System.out.println(task);
        }
    }
}
