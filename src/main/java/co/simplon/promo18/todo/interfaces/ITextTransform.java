package co.simplon.promo18.todo.interfaces;

public interface ITextTransform {
    String transform(String text);
}
