package co.simplon.promo18.todo.interfaces;

public class ColorTransformer implements ITextTransform {

    @Override
    public String transform(String text) {
        return "\u001B[31m"+text+"\u001B[0m";
    }
    
}
