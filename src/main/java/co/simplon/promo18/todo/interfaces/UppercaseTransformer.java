package co.simplon.promo18.todo.interfaces;

public class UppercaseTransformer implements ITextTransform {

    @Override
    public String transform(String text) {
        
        return text.toUpperCase();
    }
    
}
