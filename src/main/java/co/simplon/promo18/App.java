package co.simplon.promo18;

import co.simplon.promo18.example.Animal;
import co.simplon.promo18.example.Dog;
import co.simplon.promo18.example.IPond;
import co.simplon.promo18.example.Poisson;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        // Dog fido = new Dog("daschund");
        // fido.manger();
        // fido.aboyer();

        Animal animal = new Dog("corgi");
        animal.manger();

        animal = new Poisson();
        animal.manger();

        IPond pondeur = new Poisson();
        pondeur.pondre();

        Poisson poisson = new Poisson();
        
    }
}
