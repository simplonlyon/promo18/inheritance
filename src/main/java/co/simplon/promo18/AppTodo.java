package co.simplon.promo18;

import co.simplon.promo18.todo.TodoList;
import co.simplon.promo18.todo.interfaces.ColorTransformer;
import co.simplon.promo18.todo.interfaces.UppercaseTransformer;

public class AppTodo {
    public static void main(String[] args) {
        TodoList list = new TodoList(false);
        list.addTransformer(new UppercaseTransformer());
        list.addTransformer(new ColorTransformer());

        list.addTask("Faire un truc");
        list.addTask("Faire un autre truc");
        list.display();
    }
}
